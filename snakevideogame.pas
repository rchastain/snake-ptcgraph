
unit SnakeVideoGame;

interface

uses
  SysUtils,
  Classes,
  ptcCrt,
  SnakeGame,
  Graphics,
  Sound,
  HighScores;

type
  TSnakeVideoGame = class
    fSnakeGame: TSnakeGame;
    fTime: integer;
    fTwoFingersControl: boolean;
    fHighScores: THighScores;
    constructor Create;
    destructor Destroy; override;
    procedure Execute;
    procedure DrawSnake;
    procedure NewApple;
    function GetUserName: string;
    function ComputeScore: integer;
    procedure CheckArrowKey(const aKey: char);
  end;

implementation

{$INCLUDE keys.inc}

constructor TSnakeVideoGame.Create;
const
  cInitialTime = 120;
begin
  inherited Create;
  fTime := cInitialTime;
  fTwoFingersControl := FALSE;
  fSnakeGame := TSnakeGame.Create;
  LoadHighScores(fHighScores);
end;

destructor TSnakeVideoGame.Destroy;
begin
  fSnakeGame.Free;
  inherited Destroy;
end;

procedure TSnakeVideoGame.Execute;
var
  lKey: char;
  lGameOver: boolean;
  lScore: integer;
  
  function MyReadKey: char;
  begin
    result := ReadKey;
    if result = #0 then
    begin
      Readkey;
      result := MyReadKey;
    end;
  end;
  
  procedure OnKeyPress;
  begin
    lKey := ReadKey;
    case UpCase(lKey) of
      #0: CheckArrowKey(ReadKey);
      'P': ReadKey;
      ESC, CLOSE_BUTTON: lGameOver := TRUE;
    end;
  end;
  
  procedure OnCollision;
  begin
    Sound.Play(sndDie);
    while KeyPressed do
      ReadKey;
    DisplayGameOver;
    MyReadKey;
    lGameOver := TRUE;
  end;
  
  procedure OnEat;
  begin
    Sound.Play(sndEat);
    fSnakeGame.SnakeGrows;
    fSnakeGame.MoveSnake;
    DrawCurrentScore(ComputeScore);
    NewApple;
  end;
  
  procedure OnNotEat;
  begin
    fSnakeGame.SaveTailPosition;
    fSnakeGame.MoveSnake;
{$IFDEF Pictures}
    DrawGrass(fSnakeGame.fPreviousTailPosition);
{$ELSE}
    DrawPoint(fSnakeGame.fPreviousTailPosition, colorBackground, FALSE);
{$ENDIF}
  end;
  
  procedure OnHighScore;
  begin
    Sound.Play(sndHighScore);
    while KeyPressed do
      ReadKey;
    fHighScores[10].fScore := lScore;
    fHighScores[10].fName := GetUserName;
    fHighScores[10].fDate := FormatDateTime('yyyy-mm-dd', Now);
    SortHighScores(fHighScores);
    SaveHighScores(fHighScores);
  end;
  
begin
  OpenWindow;
  DisplayTitle;
  MyReadKey;
  
  lGameOver := FALSE;
  repeat
    if lGameOver then
    begin
      fSnakeGame.Free;
      fSnakeGame := TSnakeGame.Create;
      lGameOver := FALSE;
    end;
    
    DrawCurrentScore(0);
    DisplaySnakeField;
    NewApple;
    
    while not lGameOver do
    begin
      if KeyPressed then
        OnKeyPress;
      if not lGameOver then
        if fSnakeGame.Collision(FALSE) then (* FALSE = Mode sans murs. *)
          OnCollision
        else
        begin
          if fSnakeGame.SnakeEats then
            OnEat
          else
            OnNotEat;
          DrawSnake;
          Delay(fTime);
        end;
    end;
    
    lScore := ComputeScore;
    if lScore > fHighScores[10].fScore then
      OnHighScore;
    
    DisplayHighScores(fHighScores);
  until UpCase(MyReadKey) <> 'R';
  
  CloseWindow;
end;

procedure TSnakeVideoGame.DrawSnake;
var
  i: integer;
begin
{$IFDEF Pictures}
  (* Tête *)
  DrawSnakeHead(fSnakeGame.fSnake[0], fSnakeGame.fDirectionIndex);
  (* Corps *)
  for i := 1 to Pred(High(fSnakeGame.fSnake)) do
    DrawSnakeBody(
      fSnakeGame.fSnake[i],
      GetDirectionIndex(fSnakeGame.fSnake[Pred(i)], fSnakeGame.fSnake[i]),
      GetDirectionIndex(fSnakeGame.fSnake[i], fSnakeGame.fSnake[Succ(i)])
    );
  (* Queue *)
  i := High(fSnakeGame.fSnake);
  DrawSnakeTail(fSnakeGame.fSnake[i], GetDirectionIndex(fSnakeGame.fSnake[Pred(i)], fSnakeGame.fSnake[i]));
{$ELSE}
  DrawPoint(fSnakeGame.fSnake[0], colorSnake, TRUE);
{$ENDIF}
end;

procedure TSnakeVideoGame.NewApple;
begin
  if fSnakeGame.SetAppleLocation then
{$IFDEF Pictures}
    DrawApple(fSnakeGame.fApplePosition);
{$ELSE}
    DrawPoint(fSnakeGame.fApplePosition, colorApple, FALSE);
{$ENDIF}
end;

function TSnakeVideoGame.GetUserName: string;
const
  MAXLEN = 8;
var
  lInput: string;
  lKey: char;
begin
  lInput := '';
  DisplayInput(lInput);
  
  repeat
    lKey := ReadKey;
    if lKey = #0 then
    begin
      ReadKey;
      Continue;
    end;
    if lKey in ['A'..'Z', 'a'..'z'] then
      lInput := Concat(
        lInput,
        UpCase(lKey)
      )
    else
      if (lKey = BACKSPACE)
      and (Length(lInput) >= 1) then
        Delete(
          lInput,
          Length(lInput),
          1
        );
    
    DisplayInput(lInput);
  until (lKey in [ENTER, ESC, CLOSE_BUTTON]) or (Length(lInput) = MAXLEN);
  
  result := lInput;
end;

function TSnakeVideoGame.ComputeScore: integer;
begin
  result := 10 * (Length(fSnakeGame.fSnake) - INITIAL_SNAKE_LENGTH);
end;

procedure TSnakeVideoGame.CheckArrowKey(const aKey: char);
const
  LEFT = FALSE;
  RIGHT = TRUE;
begin
  if fTwoFingersControl then
    case aKey of
      ARROW_LEFT:  fSnakeGame.ChangeDirection(LEFT);
      ARROW_RIGHT: fSnakeGame.ChangeDirection(RIGHT);
    end
  else
    case aKey of
      ARROW_UP:    fSnakeGame.ChangeDirection(0);
      ARROW_RIGHT: fSnakeGame.ChangeDirection(1);
      ARROW_DOWN:  fSnakeGame.ChangeDirection(2);
      ARROW_LEFT:  fSnakeGame.ChangeDirection(3);
    end;
end;

end.
