
# Overwrite the language file.
cp -f english.inc language.inc
#cp -f french.inc language.inc

# Compile with custom-sized window (-dUserMode) and without sound.
/home/roland/Applications/fpcupdeluxe/331/fpc/bin/x86_64-linux/fpc.sh \
-dRELEASE \
-dUserMode \
-dNoSound \
@extrafpc.cfg \
snake.pas

# Use UPX to reduce executable file size.
upx snake
