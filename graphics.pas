
unit Graphics;

{$MODESWITCH ADVANCEDRECORDS+} 
{.$DEFINE UserMode} // Enable this if your ptcGraph version has the InstallUserMode function.

interface

uses
  SysUtils,
  Classes,
  StrUtils,
  ptcGraph,
  ptcCrt,
  HighScores;

type
  TPointColor = (colorSnake, colorApple, colorBackground);
  TScreenArea = (areaLabel1, areaLabel2, areaMain);
  
procedure Settings;
procedure OpenWindow;
procedure SetDrawingContext(const aScreenArea: TScreenArea; const aClearViewPort: boolean);
procedure DrawLabel(const aScreenArea: TScreenArea; const aText: string);
procedure DisplayTitle;
procedure DisplaySnakeField;
procedure DisplayGameOver;
procedure DisplayInput(const aInput: string);
procedure DisplayHighScores(const aHighScores: THighScores);
procedure DrawPoint(const aSquare: TPoint; const aColor: TPointColor; const aUpdateScreen: boolean);
procedure DrawCurrentScore(const aScore: integer);
procedure CloseWindow;

procedure CreateImages;
procedure FreeImages;
procedure DrawSnakeHead(const aLocation: TPoint; const aDirectionIndex: integer);
procedure DrawSnakeTail(const aLocation: TPoint; const aDirectionIndex: integer);
procedure DrawSnakeBody(const aLocation: TPoint; const aDirectionIndex1, aDirectionIndex2: integer);
procedure DrawGrass(const aLocation: TPoint);
procedure DrawApple(const aLocation: TPoint);
procedure DrawSnakeField;

implementation

uses
  Size,
  Colors,
  SnakePictures,
  Image,
  Utils;

{$INCLUDE language.inc}

const
  HEAD_NORTH = 0;
  HEAD_EAST = 1;
  HEAD_SOUTH = 2;
  HEAD_WEST = 3;
  TAIL_NORTH = 4;
  TAIL_EAST = 5;
  TAIL_SOUTH = 6;
  TAIL_WEST = 7;
  BODY_1 = 8;
  BODY_2 = 9;
  BODY_3 = 10;
  BODY_4 = 11;
  BODY_VERT = 12;
  BODY_HORIZ = 13;
  RABBIT = 14;
  GRASS = 15;
  
var
  lFieldWidth: integer; (* Largeur et hauteur du carré de jeu, en pixels. *)
  lSquareWidth: integer; (* Largeur et hauteur des cases dont se compose le carré de jeu. *)
  lLabelHeight: integer;
  lImages: array[0..15] of TImage;

type
  TDrawContext = record
    fTop, fBottom, fTextSize: integer;
    fColor, fBkColor: longword;
    constructor Create(const aTop, aBottom, aTextSize: integer; const aColor, aBkColor: longword);
  end;

constructor TDrawContext.Create(const aTop, aBottom, aTextSize: integer; const aColor, aBkColor: longword);
begin
  fTop := aTop;
  fBottom := aBottom;
  fTextSize := aTextSize;
  fColor := aColor;
  fBkColor := aBkColor;
end;

var
  lContextArray: array[TScreenArea] of TDrawContext;
  
procedure Settings;
begin
  lFieldWidth := 320;
  lSquareWidth := lFieldWidth div XMAX;
  lLabelHeight := 32;
  lContextArray[areaLabel1].Create(0, lLabelHeight - 1, 1, cLightYellow, cMidnightBlue);
  lContextArray[areaLabel2].Create(lLabelHeight + lFieldWidth, 2 * lLabelHeight + lFieldWidth - 1, 1, cLightYellow, cMidnightBlue);
  lContextArray[areaMain].Create(lLabelHeight, lLabelHeight + lFieldWidth - 1, 1, cGold, cForestGreen);
end;

procedure OpenWindow;
var
  lDriver, lMode: smallint;
  lErr: integer;
begin
{$IFDEF UserMode}
  lDriver := VESA;
  lMode := InstallUserMode(lFieldWidth, lFieldWidth + 2 * lLabelHeight, 16777216, 1, 10000, 10000);
  if lMode < 0 then
  begin
    Log(GraphErrorMsg(lMode));
    Halt(1);
  end;
{$ELSE}
  lDriver := VESA;
  lMode := 274; { 10 274 640x480 16777216 }
{$ENDIF}
  WindowTitle := GAME_TITLE;
  InitGraph(lDriver, lMode, '');
  lErr := GraphResult;
  if lErr <> grOk then
  begin
    Log(GraphErrorMsg(LErr));
    Halt(1);
  end;
  CreateImages;
  SetBkColor(cForestGreen);
  ClearDevice;
end;

procedure SetDrawingContext(const aScreenArea: TScreenArea; const aClearViewPort: boolean);
const
{$IFDEF UserMode}
  cScreenLeft = 0;
  cScreenTop = 0;
{$ELSE}
  cScreenLeft = 160;
  cScreenTop = 48;
{$ENDIF}
begin
  with lContextArray[aScreenArea] do
  begin
    SetViewPort(cScreenLeft, fTop + cScreenTop, Pred(lFieldWidth + cScreenLeft), Pred(fBottom + cScreenTop), ClipOff);
    SetColor(fColor);
    SetBkColor(fBkColor);
    SetTextStyle(DefaultFont, HorizDir, fTextSize);
    if aClearViewPort then
      ClearViewPort;
  end;
end;

procedure DrawLabel(const aScreenArea: TScreenArea; const aText: string);
begin
  SetDrawingContext(aScreenArea, TRUE);
  OutTextXY((lFieldWidth - TextWidth(aText)) div 2, (lLabelHeight - TextHeight(aText)) div 2, aText);
  SetDrawingContext(areaMain, FALSE);
end;

procedure DisplayTitle;
var
  s: string;
  x, y: integer;
begin
  SetDrawingContext(areaLabel1, TRUE);
  SetDrawingContext(areaMain, FALSE);
{$IFDEF PICTURES}
  DrawSnakeField;
{$ENDIF}
  s := GAME_TITLE;
  SetColor(cGreenYellow);
  SetTextStyle(DefaultFont, HorizDir, 3);
  x := (lFieldWidth - TextWidth(s)) div 2;
  y := (lFieldWidth - TextHeight(s)) div 2;
  OutTextXY(Pred(x), Pred(y), s);
  SetColor(cMaroon);
  OutTextXY(x, y, s);
  SetDrawingContext(areaLabel2, TRUE);
  s := PRESS_A_KEY;
  OutTextXY((lFieldWidth - TextWidth(s)) div 2, (lLabelHeight - TextHeight(s)) div 2, s);
end;

procedure DisplaySnakeField;
begin
{$IFDEF PICTURES}
  SetDrawingContext(areaMain, FALSE);
  DrawSnakeField;
{$ELSE}
  SetDrawingContext(areaMain, TRUE);
{$ENDIF}
  SetDrawingContext(areaLabel2, TRUE);
  SetDrawingContext(areaMain, FALSE);
end;

procedure DisplayGameOver;
var
  x, y: integer;
  s: string;
begin
  SetDrawingContext(areaMain, FALSE);
  SetTextStyle(DefaultFont, HorizDir, 2);
  s := GAME_OVER;
  x := (lFieldWidth - TextWidth(s)) div 2;
  y := (lFieldWidth - TextHeight(s)) div 2;
  OutTextXY(Pred(x), Pred(y), s);
  SetColor(cMaroon);
  OutTextXY(x, y, s);
  SetDrawingContext(areaLabel2, TRUE);
  s := PRESS_A_KEY;
  OutTextXY((lFieldWidth - TextWidth(s)) div 2, (lLabelHeight - TextHeight(s)) div 2, s);
end;

procedure DisplayInput(const aInput: string);
begin
  SetDrawingContext(areaLabel2, TRUE);
  SetDrawingContext(areaLabel1, FALSE);
  DrawLabel(areaLabel1, YOUR_NAME);
  SetDrawingContext(areaMain, TRUE);
  SetTextStyle(DefaultFont, HorizDir, 2);
  OutTextXY((lFieldWidth - TextWidth(aInput)) div 2, (lFieldWidth - TextHeight(aInput)) div 2, aInput);
end;

procedure DisplayHighScores(const aHighScores: THighScores);
var
  i: integer;
begin
  SetDrawingContext(areaLabel1, FALSE);
  DrawLabel(areaLabel1, HIGH_SCORES);
  SetDrawingContext(areaMain, TRUE);
  SetTextStyle(DefaultFont, HorizDir, 1);
  for i := Low(THighScores) to High(THighScores) do
    with aHighScores[i] do
    begin
      OutTextXY( 0 * 8 + 60, 80 + Pred(i) * 16, IfThen(fName <> '', fName, '...'));
      OutTextXY( 9 * 8 + 60, 80 + Pred(i) * 16, Format('%0.5d', [fScore]));
      OutTextXY(15 * 8 + 60, 80 + Pred(i) * 16, fDate);
    end;
end;

procedure DrawPoint(const aSquare: TPoint; const aColor: TPointColor; const aUpdateScreen: boolean);
var
  x, y: integer;
  lColor, lFillColor: longword;
begin
  case aColor of
    colorSnake:
      begin
        lColor := cLimeGreen;
        lFillColor := cDarkOrange;
      end;
    colorApple:
      begin
        lColor := cDarkOrange;
        lFillColor := cDarkRed;
      end;
    colorBackground:
      begin
        lColor := cForestGreen;
        lFillColor := cForestGreen;
      end;
  end;
  SetColor(lColor);
  SetFillStyle(SolidFill, lFillColor);
  x := lSquareWidth * (aSquare.x - 1);
  y := lFieldWidth - (lSquareWidth * aSquare.y);
  Bar(x + 1, y + 1, x + lSquareWidth - 1, y + lSquareWidth - 1);
end;

procedure DrawCurrentScore(const aScore: integer);
begin
  DrawLabel(areaLabel1, Format('%0.5d', [aScore]));
end;

procedure CloseWindow;
begin
  FreeImages;
  CloseGraph;
end;

procedure CreateImages;
var
  i: integer;
  lFileName: TFileName;
  x, y, x0: integer;
begin
  for i := 0 to 15 do
  begin
    lImages[i] := TImage.Create(16, 16);
    lFileName := Format('resources' + DirectorySeparator + 'snake_16x16_%0.2d.bmp', [i]);
    if FileExists(lFileName) then
      lImages[i].Load(lFileName)
    else
    begin
      Log(Format('File not found: %s', [lFileName]));
      Log('Creating picture...');
      (* ------------------------------------------------------------ *)
      (* Création des fichiers *.bmp à partir des données contenues   *)
      (* dans l'unité SnakePictures.                                  *)
      (* ------------------------------------------------------------ *)
      x0 := 16 * i;
      (* Dessin de l'herbe.                                           *)
      for y := 0 to 15 do
        for x := 0 to 15 do
          PutPixel(x0 + x, y, CPictures[15, y, x]);
      (* Dessin de l'image particulière.                              *)
      if i < 15 then
      begin
        for y := 0 to 15 do
          for x := 0 to 15 do
            if CPictures[i, y, x] <> $000000 then
              PutPixel(x0 + x, y, CPictures[i, y, x]);
      end;
      (* Copie de l'image dans le tableau.                            *)
      lImages[i].Get(x0, 0);
      (* Enregistrement de l'image dans un fichier.                   *)
      lImages[i].Save(lFileName);
      (* ------------------------------------------------------------ *)
    end;
  end;
end;

procedure FreeImages;
var
  i: integer;
begin
  for i := 0 to 15 do
  begin
    lImages[i].Free;
  end;
end;

procedure DrawSnakeHead(const aLocation: TPoint; const aDirectionIndex: integer);
var
  x, y: integer;
begin
  x := lSquareWidth * (aLocation.x - 1);
  y := lFieldWidth - (lSquareWidth * aLocation.y);
  lImages[aDirectionIndex + HEAD_NORTH].Put(x, y);
end;

procedure DrawSnakeTail(const aLocation: TPoint; const aDirectionIndex: integer);
var
  x, y: integer;
begin
  x := lSquareWidth * (aLocation.x - 1);
  y := lFieldWidth - (lSquareWidth * aLocation.y);
  lImages[aDirectionIndex + TAIL_NORTH].Put(x, y);
end;

procedure DrawSnakeBody(const aLocation: TPoint; const aDirectionIndex1, aDirectionIndex2: integer);
var
  x, y: integer;
  i: integer;
begin
  x := lSquareWidth * (aLocation.x - 1);
  y := lFieldWidth - (lSquareWidth * aLocation.y);
  i := 0;
  
  if aDirectionIndex1 = aDirectionIndex2 then
    if (aDirectionIndex1 = 0) or (aDirectionIndex1 = 2) then
      i := BODY_VERT
    else
      i := BODY_HORIZ
  else
    if ((aDirectionIndex1 = 0) and (aDirectionIndex2 = 3))
    or ((aDirectionIndex1 = 1) and (aDirectionIndex2 = 2)) then
      i := BODY_1
    else
      if ((aDirectionIndex1 = 1) and (aDirectionIndex2 = 0))
      or ((aDirectionIndex1 = 2) and (aDirectionIndex2 = 3)) then
        i := BODY_2
      else
        if ((aDirectionIndex1 = 2) and (aDirectionIndex2 = 1))
        or ((aDirectionIndex1 = 3) and (aDirectionIndex2 = 0)) then
          i := BODY_3
        else
          if ((aDirectionIndex1 = 3) and (aDirectionIndex2 = 2))
          or ((aDirectionIndex1 = 0) and (aDirectionIndex2 = 1)) then
            i := BODY_4;
          
  lImages[i].Put(x, y);
end;

procedure DrawGrass(const aLocation: TPoint);
var
  x, y: integer;
begin
  x := lSquareWidth * (aLocation.x - 1);
  y := lFieldWidth - (lSquareWidth * aLocation.y);
  lImages[GRASS].Put(x, y);
end;

procedure DrawApple(const aLocation: TPoint);
var
  x, y: integer;
begin
  x := lSquareWidth * (aLocation.x - 1);
  y := lFieldWidth - (lSquareWidth * aLocation.y);
  lImages[RABBIT].Put(x, y);
end;

procedure DrawSnakeField;
var
  x, y: integer;
begin
  for x := 0 to Pred(XMAX) do
    for y := 0 to Pred(YMAX) do
      lImages[GRASS].Put(lSquareWidth * x, lSquareWidth * y);
end;

initialization
  Settings;

end.
