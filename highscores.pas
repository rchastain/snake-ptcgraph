
unit HighScores;

interface

type
  TScore = record
    fScore: integer;
    fName, fDate: ansistring;
  end;
  
  THighScores = array[1..10] of TScore;
  
procedure InitHighScores(var aScores: THighScores);
procedure SortHighScores(var aScores: THighScores);
procedure LoadHighScores(var aScores: THighScores);
procedure SaveHighScores(const aScores: THighScores);

implementation

uses
  SysUtils;

const
  cFileName = 'snakehigh.dat';
  
procedure InitHighScores(var aScores: THighScores);
var
  i: integer;
begin
  for i := Low(THighScores) to High(THighScores) do
  begin
    aScores[i].fScore := 0;
    aScores[i].fName := '';
    aScores[i].fDate := FormatDateTime('yyyy-mm-dd', Now);
  end;
end;

procedure SortHighScores(var aScores: THighScores);
var
  lDone: boolean;
  i, vScore1: integer;
  lDate, lName: ansistring;
begin
  repeat
    lDone := TRUE;
    for i := Low(THighScores) to Pred(High(THighScores)) do
      if aScores[i].fScore < aScores[Succ(i)].fScore then
      begin
        vScore1 := aScores[i].fScore;
        lName := aScores[i].fName;
        lDate := aScores[i].fDate;
        aScores[i].fScore := aScores[Succ(i)].fScore;
        aScores[i].fName := aScores[Succ(i)].fName;
        aScores[i].fDate := aScores[Succ(i)].fDate;
        aScores[Succ(i)].fScore := vScore1;
        aScores[Succ(i)].fName := lName;
        aScores[Succ(i)].fDate := lDate;
        lDone := FALSE;
      end;
  until lDone;
end;

procedure LoadHighScores(var aScores: THighScores);
var
  vFile: text;
  i: integer;
begin
  InitHighScores(aScores);
  
  if not FileExists(cFileName) then
    SaveHighScores(aScores);
  
  Assign(vFile, cFileName);
  Reset(vFile);
  for i := Low(THighScores) to High(THighScores) do
  begin
    ReadLn(vFile, aScores[i].fScore);
    ReadLn(vFile, aScores[i].fName);
    ReadLn(vFile, aScores[i].fDate);
  end;
  Close(vFile);
end;

procedure SaveHighScores(const aScores: THighScores);
var
  vFile: text;
  i: integer;
begin
  Assign(vFile, cFileName);
  Rewrite(vFile);
  for i := Low(THighScores) to High(THighScores) do
  begin
    WriteLn(vFile, aScores[i].fScore);
    WriteLn(vFile, aScores[i].fName);
    WriteLn(vFile, aScores[i].fDate);
  end;
  Close(vFile);
end;

end.
