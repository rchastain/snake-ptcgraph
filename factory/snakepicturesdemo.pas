
program snakepicturesdemo;

uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  Colors24,
  SnakePictures,
  ImageUtils;

var
  gd, gm : smallint;
  
{ 10 516 640x360 16777216 }
  
  i, x, y, x0, y0: integer;
  
begin
  gd := 10;
  gm := 516;
  Initgraph(gd, gm, '');
  SetBkColor(cForestGreen);
  ClearViewPort;
  (*
  SetFillStyle(SolidFill, cBlack);
  Bar(0, 0, 63, 63);
  *)
  for i := 0 to 15 do
  begin
    x0 := 16 * (i mod 4);
    y0 := 16 * (i div 4);
    for y := 0 to 15 do
      for x := 0 to 15 do
        PutPixel(x0 + x, y0 + y, CPictures[i, y, x]);
  end;
  (*
  ImageCopy(0, 0, 15, 15, 64,  0, CopyPut);
  ImageCopy(0, 0, 15, 15, 64, 16,  XorPut);
  ImageCopy(0, 0, 15, 15, 64, 32,   OrPut);
  ImageCopy(0, 0, 15, 15, 64, 48,  AndPut);
  ImageCopy(0, 0, 15, 15, 64, 64,  NotPut);
  *)
  
  for i := 0 to 15 do
  begin
    x0 := 16 * (i mod 4);
    y0 := 16 * (i div 4);
    ImageCopy(48, 48, 63, 63, x0, y0 + 64, CopyPut);
    for y := 0 to 15 do
      for x := 0 to 15 do
      begin
        if CPictures[i, y, x] <> $000000 then
          PutPixel(x0 + x, y0 + y + 64, CPictures[i, y, x]);
      end;
  end;
  
  ImageSave(0, 0, 15, 15, 'image1.bmp');
  ImageLoad(0, 0, 15, 15, 0, 128, 'image1.bmp', CopyPut);
  
  ReadKey;
  Closegraph;
end.
