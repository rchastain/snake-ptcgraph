
uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph;
  
var
  w: word;
  f: file;
  d, m: smallint;
  
begin
  Assign(f, 'image1.bmp');
  Reset(f, 1);
  w := FileSize(f);
  WriteLn(w); // 1036
  Close(f);
  
  d := 10;
  m := 276; // 65536
  InitGraph(d, m, '');
  w := ImageSize(0, 0, 15, 15);
  CloseGraph;
  WriteLn(w); // 524
  // 524 - 16 * 16 * 2 = 12
  
  d := 10;
  m := 277; // 16777216
  InitGraph(d, m, '');
  w := ImageSize(0, 0, 15, 15);
  CloseGraph;
  WriteLn(w); // 1036
  // 1036 - 16 * 16 * 4 = 12
end.
