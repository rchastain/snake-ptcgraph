unit demo_u;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Spin, BGRABitmap, BGRABitmapTypes;

type
  { TForm1 }
  TForm1 = class(TForm)
    SaveButton: TButton;
    procedure SaveButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    { private declarations }
    FBitmap: TBGRABitmap;
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var
  LLoader: TBGRABitmap;
begin
  FBitmap := TBGRABitmap.Create(32, 32, BGRAPixelTransparent);

  LLoader := TBGRABitmap.Create('04.bmp'); LLoader.ReplaceColor(LLoader.GetPixel(0, 0), BGRAPixelTransparent);
  FBitmap.PutImage(0, 16, LLoader, dmDrawWithTransparency);
  LLoader.Free;

  LLoader := TBGRABitmap.Create('09.bmp'); LLoader.ReplaceColor(LLoader.GetPixel(0, 0), BGRAPixelTransparent);
  FBitmap.PutImage(0, 0, LLoader, dmDrawWithTransparency);
  LLoader.Free;

  LLoader := TBGRABitmap.Create('10.bmp'); LLoader.ReplaceColor(LLoader.GetPixel(0, 0), BGRAPixelTransparent);
  FBitmap.PutImage(16, 0, LLoader, dmDrawWithTransparency);
  LLoader.Free;

  LLoader := TBGRABitmap.Create('02.bmp'); LLoader.ReplaceColor(LLoader.GetPixel(0, 0), BGRAPixelTransparent);
  FBitmap.PutImage(16, 16, LLoader, dmDrawWithTransparency);
  LLoader.Free;
end;

procedure TForm1.SaveButtonClick(Sender: TObject);
begin
  FBitmap.SaveToFile('icon.png');
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FBitmap.Free;
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  FBitmap.Draw(Canvas, 10, 10, FALSE);
end;

end.

