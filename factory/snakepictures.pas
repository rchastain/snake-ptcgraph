
unit SnakePictures;

interface

type
  TPictures = array[0..15, 0..15, 0..15] of longword;
  
const
  CPictures: TPictures = (
    ({$I 00.pas}),
    ({$I 01.pas}),
    ({$I 02.pas}),
    ({$I 03.pas}),
    ({$I 04.pas}),
    ({$I 05.pas}),
    ({$I 06.pas}),
    ({$I 07.pas}),
    ({$I 08.pas}),
    ({$I 09.pas}),
    ({$I 10.pas}),
    ({$I 11.pas}),
    ({$I 12.pas}),
    ({$I 13.pas}),
    ({$I 14.pas}),
    ({$I 15.pas})
  );

implementation

end.
