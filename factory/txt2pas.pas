 
program txt2pas;

uses
  SysUtils, Classes, RegExpr;

var
  list: TStringList;
  expr: TRegExpr;
  i, j: integer;
  
begin
  expr := TRegExpr.Create('#([\dA-H]{6})');
  list := TStringList.Create;
  list.LoadFromFile({'00.txt'}ParamStr(1));
  
  for i := 1 to list.Count - 1 do
    if expr.Exec(list[i]) then
    begin
      j := i mod 16;
      if j = 1 then Write('  (') else {if j > 0 then} Write(', ');
      Write('$', expr.Match[1]);
      if j = 0 then
      begin
        Write(')');
        if i < 256 then
          WriteLn(',')
        else
          WriteLn;
      end;
    end;
  
  list.Free;
  expr.Free;
end.
