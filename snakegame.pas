
unit SnakeGame;

interface

uses
  SysUtils,
  Classes,
  Size;
  
type
  TSnake = array of TPoint;
  
  TSnakeGame = class
    fSnake: TSnake;
    fDirection, fPreviousTailPosition, fApplePosition: TPoint;
    fDirectionIndex: integer;
    constructor Create;
    destructor Destroy; override;
    procedure MoveSnake;
    procedure ChangeDirection(const aDirectionIndex: integer); overload;
    procedure ChangeDirection(const aRight: boolean); overload;
    function IsSnake(const aPoint: TPoint): boolean;
    function IsApple(const aPoint: TPoint): boolean;
    function SetAppleLocation: boolean;
    function Collision(const aWalls: boolean): boolean;
    function SnakeEats: boolean;
    procedure SnakeGrows;
    procedure SaveTailPosition;
  end;

function OppositeDirections(const aPoint1, aPoint2: TPoint): boolean;
function GetDirectionIndex(const aPoint1, aPoint2: TPoint): integer;

const
  INITIAL_SNAKE_LENGTH = 4;

implementation

uses
  Utils;

const
  DIRECTIONS: array[0..3] of TPoint = (
    (x: 0; y: 1),
    (x: 1; y: 0),
    (x: 0; y:-1),
    (x:-1; y: 0)
  );

function OppositeDirections(const aPoint1, aPoint2: TPoint): boolean;
begin
  result := (aPoint1.x = -1 * aPoint2.x) and (aPoint1.y = -1 * aPoint2.y);
end;

function GetDirectionIndex(const aPoint1, aPoint2: TPoint): integer;
begin
  (*
  if aPoint1.x = aPoint2.x then
    if aPoint1.y > aPoint2.y then result := 0 else result := 2 
  else
    if aPoint1.x > aPoint2.x then result := 1 else result := 3;
  *)
  
  if aPoint1.x = aPoint2.x then
    
    if aPoint1.y = Succ(aPoint2.y) then
      result := 0
    else if aPoint1.y = Pred(aPoint2.y) then
      result := 2
    else if aPoint1.y > aPoint2.y then
      result := 2
    else
      result := 0
  
  else { aPoint1.y = aPoint2.y }
    
    if aPoint1.x = Succ(aPoint2.x) then
      result := 1
    else if aPoint1.x = Pred(aPoint2.x) then
      result := 3
    else if aPoint1.x > aPoint2.x then
      result := 3
    else
      result := 1;
end;

constructor TSnakeGame.Create;
begin
  inherited Create;
  fDirectionIndex := 1;
  fDirection := DIRECTIONS[fDirectionIndex];
  SetLength(fSnake, INITIAL_SNAKE_LENGTH);
  fSnake[0].SetLocation(5, 5);
  fSnake[1].SetLocation(fSnake[0].Subtract(fDirection));
end;

destructor TSnakeGame.Destroy;
begin
  SetLength(fSnake, 0);
  inherited Destroy;
end;

procedure TSnakeGame.MoveSnake;
begin
  { Déplacer le corps du serpent en fonction de la position antérieure de la tête. }
  Move(fSnake[0], fSnake[1], High(fSnake) * SizeOf(TPoint));
  { Déplacer la tête en fonction de la direction courante. }
  fSnake[0].Offset(fDirection);
  { Si la tête est sortie de la surface de jeu, la téléporter de l'autre côté. Utile seulement si la
  téléportation est autorisée, autrement dit si la fonction Collision a été appelée avec la valeur
  FALSE, c'est-à-dire en mode "sans murs". }
  if fSnake[0].x > XMAX then fSnake[0].x := XMIN;
  if fSnake[0].x < XMIN then fSnake[0].x := XMAX;
  if fSnake[0].y > YMAX then fSnake[0].y := YMIN;
  if fSnake[0].y < YMIN then fSnake[0].y := YMAX;
end;

procedure TSnakeGame.ChangeDirection(const aDirectionIndex: integer);
begin
  if not OppositeDirections(fDirection, DIRECTIONS[aDirectionIndex]) then
  begin
    fDirection := DIRECTIONS[aDirectionIndex];
    if fDirectionIndex <> aDirectionIndex then
      fDirectionIndex := aDirectionIndex;
  end;
end;

procedure TSnakeGame.ChangeDirection(const aRight: boolean);
begin
  if aRight then
  begin
    Inc(fDirectionIndex);
    if fDirectionIndex > High(DIRECTIONS) then
      fDirectionIndex := Low(DIRECTIONS);
  end else
  begin
    Dec(fDirectionIndex);
    if fDirectionIndex < Low(DIRECTIONS) then
      fDirectionIndex := High(DIRECTIONS);
  end;
  ChangeDirection(fDirectionIndex);
end;

function TSnakeGame.IsSnake(const aPoint: TPoint): boolean;
var
  lSnakePart: integer;
  lTail: integer;
begin
  result := FALSE;
  lSnakePart := 1;
  lTail := High(fSnake);
  while (lSnakePart <= lTail) and not result do
  begin
    result := aPoint = fSnake[lSnakePart];
    Inc(lSnakePart);
  end;
end;

function TSnakeGame.IsApple(const aPoint: TPoint): boolean;
begin
  result := aPoint = fApplePosition;
end;

function TSnakeGame.SetAppleLocation: boolean;
(* À améliorer. *)
const
  MAX_ATTEMPT = 100;
var
  lCount: integer;
begin
  result := FALSE;
  lCount := 0;
  repeat
    Inc(lCount);
    fApplePosition.SetLocation(Random(XMAX) + XMIN, Random(YMAX) + YMIN);
    result := not (IsSnake(fApplePosition) or (fApplePosition = fPreviousTailPosition));
  until result or (lCount > MAX_ATTEMPT);
  
  if not result then
    Log('Failed to set apple location.');
end;

function TSnakeGame.Collision(const aWalls: boolean): boolean;
begin
  result := IsSnake(fSnake[0].Add(fDirection));
  
  if aWalls then
    result := result
    or (fSnake[0].x + fDirection.x > XMAX)
    or (fSnake[0].x + fDirection.x < XMIN)
    or (fSnake[0].y + fDirection.y > YMAX)
    or (fSnake[0].y + fDirection.y < YMIN);
end;

function TSnakeGame.SnakeEats: boolean;
begin
  result := IsApple(fSnake[0].Add(fDirection));
end;

procedure TSnakeGame.SnakeGrows;
begin
  SetLength(fSnake, Succ(Length(fSnake)));
end;

procedure TSnakeGame.SaveTailPosition;
var
  lTail: integer;
begin
  lTail := High(fSnake);
  fPreviousTailPosition.SetLocation(fSnake[lTail]);
end;

end.
