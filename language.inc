
const
  GAME_TITLE = 'SNAKE';
  PRESS_A_KEY = 'Press a key!';
  GAME_OVER = 'GAME OVER';
  YOUR_NAME = 'Your name?';
  HIGH_SCORES = 'High scores';
