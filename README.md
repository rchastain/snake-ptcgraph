
# Snake

Snake game for the ptcGraph unit.

## Screenshot

![screenshot](screenshots/snake2.png)

## Usage

Use the keyboard arrows to steer the snake.

## Compilation

### Library

To compile and to use the program, you need the BASS library.

The file *libbass.so* (or *bass.dll* on Windows) must be in the program directory.

The files *libbass.so* and *bass.dll* included in this repository are for a 64-bit OS. (In case your need a 32-bit binary, you can find it or on the official [BASS website](http://www.un4seen.com/), or on [this page](https://www.freebasic.net/forum/viewtopic.php?t=23394)).

If you are in a hurry, compile with `-dNoSound`.

### Compilation options

To get real graphics (and not simple colored squares) you have to compile the program with `-dPictures`.

If your FPC version is recent enough, you can use `-dUserMode`, to get a custom sized window. Otherwise the program will create a 640x480 window.

See *build.sh* for a command line example.

## Credits

### Graphics

Graphics for a snake game by Eugene Loza.

* https://opengameart.org/content/snake-sprites-sound
* https://github.com/eugeneloza/SnakeGame
* https://forum.lazarus.freepascal.org/index.php/topic,34706.0.html

Cherries image by Master484.

* https://opengameart.org/content/good-fruits-m484-games

### Sounds

The Essential Retro Video Game Sound Effects Collection by Juhani Junkala.

* https://opengameart.org/content/512-sound-effects-8-bit-style
* https://juhanijunkala.com/

BASS library.

* http://www.un4seen.com/

### Ideas

I took ideas (and even some source code) in the [simple snake game](https://www.pascalgamedevelopment.com/showthread.php?32638-Another-simple-snake-game-(no-OOP)) by Markus Mangold.
