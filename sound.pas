
unit Sound;

interface

type
  TSound = (
    sndEat,
    sndDie,
    sndHighScore
  );

procedure Play(const aSound: TSound);

implementation

{$IFDEF NoSound}
procedure Play(const aSound: TSound);
begin
end;
{$ELSE}
uses
  SysUtils, Bass;

var
  lSamples: array[TSound] of HSAMPLE;

procedure Play(const aSound: TSound);
begin
  BASS_ChannelPlay(BASS_SampleGetChannel(lSamples[aSound], FALSE), FALSE);
end;

const
  cFileName: array[TSound] of string = (
    'resources' + DirectorySeparator + 'sounds' + DirectorySeparator + 'sfx_coin_double5.wav',
    'resources' + DirectorySeparator + 'sounds' + DirectorySeparator + 'sfx_sounds_impact11.wav',
    'resources' + DirectorySeparator + 'sounds' + DirectorySeparator + 'sfx_sounds_fanfare2.wav'
  );
  
var
  LSound: TSound;
  
initialization
{$IFDEF mswindows}
  BASS_Init(-1, 44100, 0, 0, nil);
{$ELSE}
  BASS_Init(-1, 44100, 0, nil, nil);
{$ENDIF}
  
  for LSound in TSound do
    if FileExists(cFileName[LSound]) then
      lSamples[LSound] := BASS_SampleLoad(FALSE, pchar(cFileName[LSound]), 0, 0, 1, 0);
      
finalization
  BASS_Free;
{$ENDIF}
end.
