
program Snake;

{$IFDEF mswindows}
{$APPTYPE GUI}
{$R snake.res}
{$ENDIF}

uses
{$IFDEF UNIX}
  CThreads,
{$ENDIF}
  SysUtils,
  SnakeVideoGame,
  Utils;

begin
  Randomize;
  Log(Concat('Snake for ptcGraph ', {$I %DATE%}, ' ', {$I %TIME%}, ' FPC ', {$I %FPCVERSION%}, ' ', {$I %FPCTARGETOS%}, ' ', {$I %FPCTARGETCPU%}), TRUE);
  with TSnakeVideoGame.Create do
  try
    Execute;
  finally
    Free;
  end;
end.
